﻿using SQLite;
using System;
using System.Collections.Generic;
using System.Text;

namespace Senacao.Models
{
    public class Agendamento
    {
        [PrimaryKey, AutoIncrement]
        public int ID { get; set; }
        public string Nome { get; set; }
        public string Celular { get; set; }
        public string Email { get; set; }
        public float PrecoTotal { get; set; }
        public string TextoPrecoTotal
        {
            get
            {
                return string.Format("Total: R$ {0}", PrecoTotal);
            }
        }

        DateTime dataAgendamento = DateTime.Today;
        public DateTime DataAgendamento
        {
            get
            {
                return dataAgendamento;
            }
            set
            {
                dataAgendamento = value;
            }
        }
        public TimeSpan HoraAgendamento { get; set; }

        public Agendamento()
        {

        }

        public Agendamento(string nome, 
            string celular, string email)
        {
            this.Nome = nome;
            this.Celular = celular;
            this.Email = email;
        }
    }
}
